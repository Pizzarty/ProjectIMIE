<?php

  Class Admin {

    protected $id;
    protected $nom;
    protected $prenom;
    protected $login;
    protected $password;

    public function getId(){
      return $this->id;
    }
    public function setId($id){
      $this->id = $id;
    }

    public function getNom(){
      return $this->nom;
    }
    public function setNom($nom){
      $this->nom = $nom;
    }

    public function getPrenom(){
      return $this->prenom;
    }
    public function setPrenom($prenom){
      $this->prenom = $prenom;
    }

    public function getlogin(){
      return $this->login;
    }
    public function setlogin($login){
      $this->login = $login;
    }

    public function getPassword(){
      return $this->password;
    }
    public function setPassword($password){
      $this->password = $password;
    }

  }
