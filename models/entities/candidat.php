<?php

Class Candidat
{

    private $id;
    private $civilite;
    private $nom;
    private $prenom;
    private $dateNaiss;
    private $cp;
    private $ville;
    private $tel1;
    private $email;
    private $diplome;
    private $etablissement;
    private $id_statut;
    private $id_site;
    private $id_souhait;
    private $id_souhait_1;
    private $id_souhait_2;
    private $formEnCours;
    private $dateSaisieForm;
    private $id_connait;
    private $dispo;

    public function insert($pdo) {

      try {

          if (!empty($_POST['nom']) && $_POST['nom'] || !empty($_POST['prenom']) && $_POST['prenom'] || !empty($_POST['datenaiss']) && $_POST['datenaiss'] || !empty($_POST['cp']) && $_POST['cp'] || !empty($_POST['ville']) && $_POST['ville'] || !empty($_POST['tel1']) && $_POST['tel1'] || !empty($_POST['email']) && $_POST['email'] || !empty($_POST['emailconfirm']) && $_POST['emailconfirm'] || !empty($_POST['diplome']) && $_POST['diplome'] || !empty($_POST['etablissement']) && $_POST['etablissement'] || !empty($_POST['dispo']) && $_POST['dispo']) {

            if(strlen($_POST['cp']) != 5 || !is_numeric($_POST['cp']))
              echo "Le code postal doit contenir 5 chiffres";

            if(strlen($_POST['tel1']) != 10 || !is_numeric($_POST['tel1']))
              echo "Le numéro de téléphone doit contennir 10 chiffres";

            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
              echo "Adresse mail non valide";

            if(($_POST['email']) != ($_POST['emailconfirm'])){
              echo "Les adresses mail ne correspondent pas";

            return;

          } else {

            $this->dateSaisieForm = new DateTime();

            $stmt = $pdo->prepare("INSERT INTO formulaire (id_civiliter, nom, prenom, date_naiss, cp, ville, tel1, email, diplome, etablissement, id_statut, id_site, id_souhait, id_souhait_1, id_souhait_2, connait, formation_en_cours, disponibilite, date_saisie) VALUES (:civilite, :nom, :prenom, :datenaiss, :cp, :ville, :tel1, :email, :diplome, :etablissement, :statut, :site, :souhait1, :souhait2, :souhait3, :connait, :formencours, :dispo, :dateSaisieForm)");

            //Binder les paramètres à la requête de manière sécurisée
            $stmt->bindParam(':civilite', $this->civilite, PDO::PARAM_STR);
            $stmt->bindParam(':nom', $this->nom, PDO::PARAM_STR);
            $stmt->bindParam(':prenom', $this->prenom, PDO::PARAM_STR);
            $stmt->bindParam(':datenaiss', $this->dateNaiss, PDO::PARAM_STR);
            $stmt->bindParam(':cp', $this->cp, PDO::PARAM_INT);
            $stmt->bindParam(':ville', $this->ville, PDO::PARAM_STR);
            $stmt->bindParam(':tel1', $this->tel1, PDO::PARAM_INT);
            $stmt->bindParam(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindParam(':diplome', $this->diplome, PDO::PARAM_STR);
            $stmt->bindParam(':etablissement', $this->etablissement, PDO::PARAM_STR);
            $stmt->bindParam(':statut', $this->id_statut, PDO::PARAM_STR);
            $stmt->bindParam(':site', $this->id_site, PDO::PARAM_STR);
            $stmt->bindParam(':souhait1', $this->id_souhait, PDO::PARAM_STR);
            $stmt->bindParam(':souhait2', $this->id_souhait_1, PDO::PARAM_STR);
            $stmt->bindParam(':souhait3', $this->id_souhait_2, PDO::PARAM_STR);
            $stmt->bindParam(':connait', $this->id_connait, PDO::PARAM_STR);
            $stmt->bindParam(':formencours', $this->formEnCours, PDO::PARAM_STR);
            $stmt->bindParam(':dispo', $this->dispo, PDO::PARAM_STR);
            $stmt->bindParam(':dateSaisieForm', $this->dateSaisieForm->format("Y-m-d H:i:s"), PDO::PARAM_STR);

            //On exécute ensuite la requête préparée
            //$stmt->execute();

            return "Formulaire de pré-inscription envoyé !";

          }

        } else {
          echo "Donnée(s) manquante";
        }

      } catch(PDOException $e) {
          return "Votre enregistrement a échoué, en voici la raison : " . $e->getMessage();
      }

    }

    public function save($pdo) {

  		//Si l'id est renseigné à l'appel de la méthode alors c'est une mise à jour, sinon $id équivaut à false et alors l'objet client actuel doit faire l'objet d'un nouvel enregistrement.
  		if($this->id) {
  			//appeler la bonne méthode
  			$message = $this->update($pdo);
  			return $message;
  		} else {
  			$message = $this->insert($pdo);
  			return $message;
  		}
  	}

    private function update($pdo) {

      try {
        //Exécuter la requête update d'une personne en base de donnée
        //Préparation de la requête
        $stmt = $pdo->prepare('UPDATE formulaire SET civilite = :id_civilite, nom = :nom, prenom = :prenom, dateNaiss = :date_naiss, cp = :cp, ville = :vile, tel = :tel1, email = :email, diplome = :diplome, etablissement = :etablissement, statut = :id_statut, site = :site, id_souhait = :id_souhait, id_souhait_1 = :id_souhait_1, id_souhait_2 = :id_souhait_2, disponibilite = :disponibilite WHERE id = :id');

        //Binder les paramètres à la requête de manière sécurisée
        $stmt->bindParam(':id_civilite', $this->civilite, PDO::PARAM_STR);
        $stmt->bindParam(':nom', $this->nom, PDO::PARAM_STR);
        $stmt->bindParam(':prenom', $this->prenom, PDO::PARAM_STR);
        $stmt->bindParam(':date_naiss', $this->dateNaiss, PDO::PARAM_STR);
        $stmt->bindParam(':cp', $this->cp, PDO::PARAM_INT);
        $stmt->bindParam(':ville', $this->ville, PDO::PARAM_STR);
        $stmt->bindParam(':tel1', $this->tel1, PDO::PARAM_INT);
        $stmt->bindParam(':email', $this->email, PDO::PARAM_STR);
        $stmt->bindParam(':diplome', $this->diplome, PDO::PARAM_STR);
        $stmt->bindParam(':etablissement', $this->etablissement, PDO::PARAM_STR);
        $stmt->bindParam(':id_statut', $this->id_statut, PDO::PARAM_STR);
        $stmt->bindParam(':id_site', $this->site, PDO::PARAM_STR);
        $stmt->bindParam(':id_souhait', $this->id_souhait, PDO::PARAM_STR);
        $stmt->bindParam(':id_souhait_1', $this->id_souhait_1, PDO::PARAM_STR);
        $stmt->bindParam(':id_souhait_2', $this->id_souhait_2, PDO::PARAM_STR);
        $stmt->bindParam(':disponibilite', $this->dispo, PDO::PARAM_STR);

        //On exécute ensuite la requête préparée
        $stmt->execute();

        return "Votre client a été mis à jour avec succès";

      }
        catch(PDOException $e) {
          return "Votre mise à jour a échoué, en voici la raison : " . $e->getMessage();
        }
    }

  	public function delete($pdo) {

  		//Supprimer un enregistrement en base de donnée
  		//Faire un try catch qui renvoie un message pour indiquer si la suppression s'est bien déroulée ou non
  		try{
  			$stmt = $pdo->prepare('DELETE FROM formulaire WHERE id = :id');
  			$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);

  			$stmt->execute();

  			return "Le candidat a bien été supprimé.";
  		}
  		catch(PDOException $e) {
  			return "Votre suppression a échoué, en voici la raison : " . $e->getMessage();

  		}

  	}

    public function getId(){
      return $this->id;
    }
    public function setId($id){
      $this->id = $id;
    }

    public function getIne(){
      return $this->ine;
    }
    public function setIne($ine){
      $this->ine = $ine;
    }

    public function getCivilite(){
      return $this->civilite;
    }
    public function setCivilite($civilite){
      $this->civilite = $civilite;
    }

    public function getNom(){
      return $this->nom;
    }
    public function setNom($nom){
      $this->nom = $nom;
    }

    public function getPrenom(){
      return $this->prenom;
    }
    public function setPrenom($prenom){
      $this->prenom = $prenom;
    }

    public function getDateNaiss(){
      return $this->dateNaiss;
    }
    public function setDateNaiss($dateNaiss){
      $this->dateNaiss = $dateNaiss;
    }

    public function getCp(){
      return $this->cp;
    }
    public function setCp($cp){
      $this->cp = $cp;
    }

    public function getVille(){
      return $this->ville;
    }
    public function setVille($ville){
      $this->ville = $ville;
    }

    public function getTel1(){
      return $this->tel1;
    }
    public function setTel1($tel1){
      $this->tel1 = $tel1;
    }

    public function getEmail(){
      return $this->email;
    }
    public function setEmail($email){
      $this->email = $email;
    }

    public function getDiplome(){
      return $this->diplome;
    }
    public function setDiplome($diplome){
      $this->diplome = $diplome;
    }

    public function getEtablissement(){
      return $this->etablissement;
    }
    public function setEtablissement($etablissement){
      $this->etablissement = $etablissement;
    }

    public function getStatut(){
      return $this->id_statut;
    }
    public function setStatut($id_statut){
      $this->id_statut = $id_statut;
    }

    public function getSite(){
      return $this->id_site;
    }
    public function setSite($id_site){
      $this->id_site = $id_site;
    }

    public function getSouhait(){
      return $this->id_souhait;
    }
    public function setSouhait($id_souhait){
      $this->id_souhait = $id_souhait;
    }

    public function getSouhait1(){
      return $this->id_souhait_1;
    }
    public function setSouhait1($id_souhait_1){
      $this->id_souhait_1 = $id_souhait_1;
    }

    public function getSouhait2(){
      return $this->id_souhait_2;
    }
    public function setSouhait2($id_souhait_2){
      $this->id_souhait_2 = $id_souhait_2;
    }

    public function getFormEnCours(){
      return $this->formEnCours;
    }
    public function setFormEnCours($formEnCours){
      $this->formEnCours = $formEnCours;
    }

    public function getDateSaisieForm(){
      return $this->dateSaisieForm;
    }
    public function setDateSaisieForm($dateSaisieForm){
      $this->dateSaisieForm = $dateSaisieForm;
    }

    public function getConnait(){
      return $this->id_connait;
    }
    public function setConnait($id_connait){
      $this->id_connait = $id_connait;
    }

    public function getDisponibilite(){
      return $this->dispo;
    }
    public function setDisponibilite($dispo){
      $this->dispo = $dispo;
    }

}
