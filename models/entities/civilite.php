<?php

Class Civilite
{

  private $id;
  private $civilite;


  public function getId(){
    return $this->id;
  }

  public function setId($id){
    $this->id = $id;
  }

  public function getCivilite(){
    return $this->civilite;
  }

  public function setCivilite($civilite){
    $this->civilite = $civilite;
  }

}
