<?php

  class AdminRepository
  {

    public function getAdmin($pdo, $login, $password) {



  		// Pas sécurisé contre les injections SQL
  		// Entrer " OR 1=1 # pour vous connectez sans connaître un login et un password d'un admin enregistré.
  		//$resultat = $pdo->query('SELECT id, nom, prenom, login, password FROM user WHERE login = "' . $login . '" AND password = "' . $password . '"');

  		$resultat = $pdo->prepare('SELECT id, nom, prenom, login, password FROM admin WHERE login = :login AND password = :password');

  		$resultat->bindParam(':login', $login, PDO::PARAM_STR);
  		$resultat->bindParam(':password', $password, PDO::PARAM_STR);
  		$resultat->setFetchMode(PDO::FETCH_OBJ);

  		$resultat->execute();

  		$obj = $resultat->fetch();

  		if(empty($obj)) {
  			return null;
  		} else {
  			$admin = new Admin();
  			$admin->setId($obj->id);
  			$admin->setNom($obj->nom);
  			$admin->setPrenom($obj->prenom);
  			$admin->setLogin($obj->login);
  			$admin->setPassword($obj->password);

  			return $admin;
  		}

  	}

    public function export($pdo){

      $resultat = $pdo->query('SELECT nom, prenom, date_naiss, cp, ville, tel1, email, diplome, etablissement, disponibilite, date_saisie, formation_en_cours, id_souhait, id_souhait_1, id_souhait_2, id_statut, id_site, connait, id_civiliter FROM formulaire');

      $chemin = '././exports/fichier.csv';

      $fp = fopen("$chemin", "w+");

      $tableau = ["Y_identifant_site", "Y_identifant_site2","Y_identifant_site3","identifiant_statut","Y_identifiant_annee","Y_Civilite_de_candidat","Y_Nom_de_candidat","ine_candidat","Y_Prenom_de_candidat","Nom_de_jeune_fille","Y_Nationalite_du_candidat","Y_1ère_ligne_de_l’adresse_«_courrier_»","2ème_ligne_de_l’adresse_«_courrier_»","3ème_ligne_de_l’adresse_«_courrier_»","4ème_ligne_de_l’adresse_«_courrier_»","Y_Code_postal_de_l’adresse_«_courrier_»","Y_Ville_de_l’adresse_«_courrier_»","Pays_de_l’adresse_«_courrier_»","Y_Telephone_1_de_l’adresse_«_courrier_»","Telephone_2_de_l’adresse_«_courrier_»","Y_Email_de_l’adresse_«_courrier_»","cilivite_resp_legal","nom_resp_legal","prenom_resp_legal","1ère_ligne_de_l’adresse_«_courrier_»","2ème_ligne_de_l’adresse_«_courrier_»","3ème_ligne_de_l’adresse_«_courrier_»","4ème_ligne_de_l’adresse_«_courrier_»","Code_postal_de_l’adresse_«_courrier_»","Ville_de_l’adresse_«_courrier_»","Pays_de_l’adresse_«_courrier_»","Telephone_1_de_l’adresse_«_courrier_»","Telephone_2_de_l’adresse_«_courrier_»","Email_de_l’adresse_«_courrier_»","Y_date_naissance_candidat","Y_lieu_naiss_candidat","Y_departement_naiss_candidat","Y_premier_souhait","deuxieme_souhait","troisieme_souhait","Y_origine_scolaire","Y_dernier_diplome","etablissement_origine","date_saisie_formulaire","url_cv_candidat","url_lettre_motiv_candidat","observation","rp1","rp2","rp3","rp1_obs","rp2_obs","rp3_obs"];

      foreach($tableau as $entete){

        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
        fputs($fp, "$entete;");

      }

      fputs($fp, "\n");

  		while($donnees = $resultat->fetch()){

        $a = $donnees['id_civiliter'];
        $b = $donnees['nom'];
        $c = $donnees['prenom'];
        $d = new DateTime($donnees['date_naiss']);
        $d = $d->format("d/m/Y");
        $e = $donnees['ville'];
        $f = $donnees['tel1'];
        $g = $donnees['email'];
        $h = $donnees['libelle'];
        $i = $donnees['cp'];
        $j = $donnees['diplome'];
        $k = $donnees['etablissement'];
        $l = $donnees['disponibilite'];
        $m = new DateTime($donnees['date_saisie']);
        $m = $m->format("d/m/Y");
        $n = $donnees['formation_en_cours'];
        $o = $donnees['id_souhait'];
        $p = $donnees['id_souhait_1'];
        $q = $donnees['id_souhait_2'];
        $r = $donnees['id_statut'];
        $s = $donnees['id_site'];
        $t = $donnees['connait'];

        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
        fputs($fp, "$s;;;$r;;$a;$b;;$c;;;;;;;$i;$e;;$f;;$g;;;;;;;;;;;;;;$d;;;$o;$p;$q;;$j;$k;$m;;;$l;;;;$t;;\n");

      }

      fclose($fp);
      $resultat->closeCursor();

    }

  }
