<?php

  class CiviliteRepository
  {

    public function getCivilite($pdo) {



  		// Pas sécurisé contre les injections SQL
  		//$resultat = $pdo->query('SELECT id, libelle FROM connait');

  		$resultat = $pdo->prepare('SELECT id, civilite FROM civiliter');

  		$resultat->setFetchMode(PDO::FETCH_OBJ);

  		$resultat->execute();

      $listConnait = array();

  		while($obj = $resultat->fetch()){

        $civilite = new Civilite();
        $civilite->setId($obj->id);
        $civilite->setCivilite($obj->civilite);

        $listCivilite[] = $civilite;

      }

      return $listCivilite;

  	}

  }
