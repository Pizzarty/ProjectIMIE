<?php

  class SiteRepository
  {

    public function getSite($pdo) {



  		// Pas sécurisé contre les injections SQL
  		//$resultat = $pdo->query('SELECT id, ville FROM site');

  		$resultat = $pdo->prepare('SELECT id, ville FROM site');

  		$resultat->setFetchMode(PDO::FETCH_OBJ);

  		$resultat->execute();

      $listSite = array();

  		while($obj = $resultat->fetch()){

        $site = new Site();
        $site->setId($obj->id);
        $site->setVille($obj->ville);

        $listSite[] = $site;

      }

      return $listSite;

  	}

  }
