<?php

  class FormationRepository
  {

    public function getFormation($pdo) {



  		// Pas sécurisé contre les injections SQL
  		//$resultat = $pdo->query('SELECT id, ville FROM statut');

  		$resultat = $pdo->prepare('SELECT id, libelle FROM souhait');

  		$resultat->setFetchMode(PDO::FETCH_OBJ);

  		$resultat->execute();

      $listFormation = array();

  		while($obj = $resultat->fetch()){

        $formation = new Formation();
        $formation->setId($obj->id);
        $formation->setLibelle($obj->libelle);

        $listFormation[] = $formation;

      }

      return $listFormation;

  	}

  }
