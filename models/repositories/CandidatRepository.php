<?php

//Les objets repository permettent de récupérer des enregistrements en base de données
//Toutes les requpetes select sont donc sensées s'y trouver

class CandidatRepository
{

	//Récupère la liste de tous les clients en base de données
	public function getAll($pdo) {

		//Effectuer la requête en bdd pour récupérer l'ensemble des clients enregistrés en bdd
					$resultats = $pdo->query('SELECT id, id_civiliter, nom, prenom, cp FROM formulaire');

		$resultats->setFetchMode(PDO::FETCH_OBJ);

		$listCandidat = array();

		while($obj = $resultats->fetch()){

			$candidat = new Candidat();
			$candidat->setId($obj->id);
			$candidat->setCivilite($obj->civilite);
			$candidat->setNom($obj->nom);
			$candidat->setPrenom($obj->prenom);
			$candidat->setCp($obj->cp);

			$listCandidat[] = $candidat;

		}

		return $listCandidat;


	}

	//Récupère un candidat en fonction de l'id renseigné
	public function getOneById($pdo, $id) {

    //Effectuer la requête en bdd pour récupérer le client correspondant à l'id renseigné
    $resultat = $pdo->query('SELECT f.id, f.nom, f.prenom, f.date_naiss, f.cp, f.ville, f.tel1, f.email, f.diplome, f.etablissement, st.libelle AS stlibelle, si.ville, ci.civilite, s.libelle, s1.libelle AS libelle1, s2.libelle AS libelle2 FROM formulaire f
            INNER join statut st ON id_statut = st.id
            INNER join site si ON id_site = si.id
            INNER JOIN souhait s ON id_souhait = s.id
            INNER JOIN souhait s1 ON id_souhait_1 = s1.id
            INNER JOIN souhait s2 ON id_souhait_2 = s2.id
						INNER JOIN civiliter ci ON id_civiliter = ci.id
						WHERE f.id = ' . $id);

    $resultat->setFetchMode(PDO::FETCH_OBJ);

    $obj = $resultat->fetch();

    //Ensuite :
    // 1 -  instancier un objet candidat
    // 2 -  hydrater ses attributs avec les valeurs récupérées en bdd
    // 3 -  retourner ensuite cet objet

    $candidat = new Candidat();
    $candidat->setId($obj->id);
    $candidat->setCivilite($obj->civilite);
    $candidat->setNom($obj->nom);
    $candidat->setPrenom($obj->prenom);
    $candidat->setDateNaiss($obj->date_naiss);
    $candidat->setCp($obj->cp);
    $candidat->setVille($obj->ville);
    $candidat->setTel1($obj->tel1);
    $candidat->setEmail($obj->email);
    $candidat->setDiplome($obj->diplome);
    $candidat->setEtablissement($obj->etablissement);
    $candidat->setStatut($obj->stlibelle);
    $candidat->setSite($obj->ville);
    $candidat->setSouhait($obj->libelle);
    $candidat->setSouhait1($obj->libelle1);
    $candidat->setSouhait2($obj->libelle2);

    return $candidat;

  }

  public function getTout($pdo, $id) {

  	//Effectuer la requête en bdd pour récupérer le client correspondant à l'id renseigné
  	$resultat = $pdo->query('SELECT f.nom, f.prenom, f.datenaiss, f.ville, f.tel1, f.email, s.libelle, si.ville, st.libelle, ci.civilite FROM formulaire f
            INNER JOIN souhait s ON id_souhait = s.id
            INNER JOIN site si ON id_site = si.id
						INNER JOIN statut st ON id_statut = st.id
						INNER JOIN civiliter ci ON id_civiliter = ci.id
						WHERE f.id = ' . $id);

  	$resultat->setFetchMode(PDO::FETCH_OBJ);

  	$listCandidat = array();

  	$obj = $resultat->fetch();


    $candidat = new Candidat();
    $candidat->setCivilite($obj->civilite);
    $candidat->setNom($obj->nom);
    $candidat->setPrenom($obj->prenom);
    $candidat->setDateNaiss($obj->datenaiss);
    $candidat->setVille($obj->ville);
    $candidat->setTel1($obj->tel1);
    $candidat->setEmail($obj->email);
    $candidat->setSouhait1($obj->libelle);
    $candidat->setSite($obj->ville);

    $listCandidat[] = $candidat;

    return $listCandidat;

  }

}
