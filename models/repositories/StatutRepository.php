<?php

  class StatutRepository
  {

    public function getStatut($pdo) {



  		// Pas sécurisé contre les injections SQL
  		//$resultat = $pdo->query('SELECT id, ville FROM statut');

  		$resultat = $pdo->prepare('SELECT id, libelle FROM statut');

  		$resultat->setFetchMode(PDO::FETCH_OBJ);

  		$resultat->execute();

      $listStatut = array();

  		while($obj = $resultat->fetch()){

        $statut = new Statut();
        $statut->setId($obj->id);
        $statut->setLibelle($obj->libelle);

        $listStatut[] = $statut;

      }

      return $listStatut;

  	}

  }
