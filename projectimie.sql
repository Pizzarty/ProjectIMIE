-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Jeu 16 Mars 2017 à 17:54
-- Version du serveur :  5.5.49-log
-- Version de PHP :  7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projectimie`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `nom`, `prenom`, `login`, `password`) VALUES
(1, 'Banville', 'Pierre', 'Pierre', 'merci');

-- --------------------------------------------------------

--
-- Structure de la table `connait`
--

CREATE TABLE IF NOT EXISTS `connait` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `connait`
--

INSERT INTO `connait` (`id`, `libelle`) VALUES
(1, 'Site Internet'),
(2, 'Evénement'),
(3, 'Affichage'),
(4, 'Presse'),
(5, 'Radio'),
(6, 'Lycée'),
(7, 'Entreprise'),
(8, 'Parents'),
(9, 'Amis/Bouche à oreille');

-- --------------------------------------------------------

--
-- Structure de la table `formulaire`
--

CREATE TABLE IF NOT EXISTS `formulaire` (
  `id` int(11) NOT NULL,
  `civilite` varchar(5) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `date_naiss` date DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `tel1` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `diplome` varchar(50) DEFAULT NULL,
  `etablissement` varchar(50) DEFAULT NULL,
  `disponibilite` varchar(50) DEFAULT NULL,
  `date_saisie` date DEFAULT NULL,
  `formation_en_cours` varchar(50) DEFAULT NULL,
  `id_souhait` int(11) DEFAULT NULL,
  `id_statut` int(11) DEFAULT NULL,
  `id_site` varchar(25) DEFAULT NULL,
  `id_souhait_1` int(11) DEFAULT NULL,
  `id_souhait_2` int(11) DEFAULT NULL,
  `id_connait` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `id` varchar(25) NOT NULL,
  `ville` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `site`
--

INSERT INTO `site` (`id`, `ville`) VALUES
('ANG', 'Angers'),
('CAE', 'Caen'),
('LAV', 'Laval'),
('LEM', 'Le Mans'),
('NAN', 'Nantes'),
('PAR', 'Paris'),
('REN', 'Rennes');

-- --------------------------------------------------------

--
-- Structure de la table `souhait`
--

CREATE TABLE IF NOT EXISTS `souhait` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `souhait`
--

INSERT INTO `souhait` (`id`, `libelle`) VALUES
(1, 'ALT - Chef de Projet Conception de SI'),
(2, 'ALT - Concepteur Dév. Projets Num.'),
(3, 'ALT - Développeur Logiciel'),
(4, 'ALT - Resp. Infrastructures Sys. Rés.'),
(5, 'ALT - Technicien Support Informatique'),
(6, 'ALT - Web Master Designer'),
(7, 'Bachelor Digital'),
(8, 'BTS SIO'),
(9, 'Digistart'),
(10, 'IT Start'),
(11, 'REG - Concepteur Développeur'),
(12, 'REG - Développeur Logiciel'),
(13, 'REG - Responsable Infrastructure'),
(14, 'REG - Technicien Réseaux é Télécoms'),
(15, 'REG - Technicien Support Informatique');

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE IF NOT EXISTS `statut` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `statut`
--

INSERT INTO `statut` (`id`, `libelle`) VALUES
(1, 'Congé Individuel Formation'),
(2, 'Contrat de Professionnalisation'),
(3, 'Contrat Sécurisation Profesionnelle'),
(4, 'Convention d''Observation'),
(5, 'Demandeur d''Emploi'),
(6, 'Etudiant'),
(7, 'Formation Professionnel'),
(8, 'Période de Professionnalisation'),
(9, 'Salarié'),
(10, 'Stagiaire');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `connait`
--
ALTER TABLE `connait`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formulaire`
--
ALTER TABLE `formulaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_formulaire_id_souhait` (`id_souhait`),
  ADD KEY `FK_formulaire_id_statut` (`id_statut`),
  ADD KEY `FK_formulaire_id_site` (`id_site`),
  ADD KEY `FK_formulaire_id_souhait_1` (`id_souhait_1`),
  ADD KEY `FK_formulaire_id_souhait_2` (`id_souhait_2`),
  ADD KEY `FK_formulaire_id_connait` (`id_connait`);

--
-- Index pour la table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `souhait`
--
ALTER TABLE `souhait`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `connait`
--
ALTER TABLE `connait`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `formulaire`
--
ALTER TABLE `formulaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `souhait`
--
ALTER TABLE `souhait`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `statut`
--
ALTER TABLE `statut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `formulaire`
--
ALTER TABLE `formulaire`
  ADD CONSTRAINT `FK_formulaire_id_connait` FOREIGN KEY (`id_connait`) REFERENCES `connait` (`id`),
  ADD CONSTRAINT `FK_formulaire_id_site` FOREIGN KEY (`id_site`) REFERENCES `site` (`id`),
  ADD CONSTRAINT `FK_formulaire_id_souhait` FOREIGN KEY (`id_souhait`) REFERENCES `souhait` (`id`),
  ADD CONSTRAINT `FK_formulaire_id_souhait_1` FOREIGN KEY (`id_souhait_1`) REFERENCES `souhait` (`id`),
  ADD CONSTRAINT `FK_formulaire_id_souhait_2` FOREIGN KEY (`id_souhait_2`) REFERENCES `souhait` (`id`),
  ADD CONSTRAINT `FK_formulaire_id_statut` FOREIGN KEY (`id_statut`) REFERENCES `statut` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
