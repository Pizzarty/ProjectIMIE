<?php

  //Contrôleur principal
  session_start();
  include_once('library/PDOFactory.php');
  include_once('models/entities/admin.php');
  include_once('models/entities/candidat.php');
  include_once('models/entities/site.php');
  include_once('models/entities/statut.php');
  include_once('models/entities/formation.php');
  include_once('models/entities/civilite.php');
  include_once('models/repositories/AdminRepository.php');
  include_once('models/repositories/CandidatRepository.php');
  include_once('models/repositories/SiteRepository.php');
  include_once('models/repositories/StatutRepository.php');
  include_once('models/repositories/FormationRepository.php');
  include_once('models/repositories/CiviliteRepository.php');

  //Connection à la base de donnée
  $pdo = PDOFactory::getMysqlConnection();


  if (isset($_REQUEST['action'])){
    $action = $_REQUEST['action'];
  } else {
    $action = null;
  }

  switch ($action) {

    case "verifLogin":
      $adminRepo = new AdminRepository();
		$admin = $adminRepo->getAdmin($pdo, $_POST['login'], $_POST['pwd']);

  		if($admin) {
  			$_SESSION['id'] = $admin->getId();
  			$_SESSION['login'] = $admin->getLogin();
  			$_SESSION['nom'] = $admin->getNom();
  			$_SESSION['prenom'] = $admin->getPrenom();
  			//On prépare la vue à afficher avec les données dont elle a besoin

  				$candidatRepo = new CandidatRepository();
  				$listCandidat = $candidatRepo->getAll($pdo);

          $siteRepo = new SiteRepository();
          $listSite = $siteRepo->getSite($pdo);

          $statutRepo = new StatutRepository();
          $listStatut = $statutRepo->getStatut($pdo);

          $formationRepo = new FormationRepository();
          $listFormation = $formationRepo->getFormation($pdo);

          $civiliteRepo = new CiviliteRepository();
          $listCivilite = $civiliteRepo->getCivilite($pdo);

  				$vueAAfficher = "formulaire.php";
  		} else {
  				$message = "Identifiants invalides !";
  				$vueAAfficher = "views/login.php";
  			}
  		break;

    case "disconnect":
    	$_SESSION = array();
    	session_destroy();
    	$vueAAfficher = "views/login.php";
    	break;

    case "candidat":
      $candidat = new Candidat();
      $candidat->setCivilite($_POST["civilite"]);
      $candidat->setNom($_POST["nom"]);
      $candidat->setPrenom($_POST["prenom"]);
      $candidat->setDateNaiss($_POST["datenaiss"]);
      $candidat->setCp($_POST["cp"]);
      $candidat->setVille($_POST["ville"]);
      $candidat->setTel1($_POST["tel1"]);
      $candidat->setEmail($_POST["email"]);
      $candidat->setDiplome($_POST["diplome"]);
      $candidat->setEtablissement($_POST["etablissement"]);
      $candidat->setStatut($_POST["statut"]);
      $candidat->setSite($_POST["site"]);
      $candidat->setSouhait($_POST["souhait1"]);
      $candidat->setSouhait1($_POST["souhait2"]);
      $candidat->setSouhait2($_POST["souhait3"]);
      $candidat->setFormEnCours($_POST["formencours"]);
      $candidat->setConnait($_POST["connait"]);
      $candidat->setDisponibilite($_POST["dispo"]);

      $message = $candidat->insert($pdo);

      $siteRepo = new SiteRepository();
      $listSite = $siteRepo->getSite($pdo);

      $statutRepo = new StatutRepository();
      $listStatut = $statutRepo->getStatut($pdo);

      $formationRepo = new FormationRepository();
      $listFormation = $formationRepo->getFormation($pdo);

      $civiliteRepo = new CiviliteRepository();
      $listCivilite = $civiliteRepo->getCivilite($pdo);

      $vueAAfficher = "formulaire.php";
      break;

      case "export":
      $export = new AdminRepository();
      $export1 = $export->export($pdo);
      $vueAAfficher = "formulaire.php";
      break;

      case "listCandidat":
      //On prépare la vue a afficher avec les données dont elle a besoin
      $candidatRepo = new CandidatRepository();
      $listCandidat = $candidatRepo->getAll($pdo);
      $vueAAfficher = "views/listCandidat.php";
      break;

      case "afficheCandidat":

       $candidatRepo = new CandidatRepository();
       $listCandidat = $candidatRepo->getTout($pdo,$_GET['id']);
       $vueAAfficher = "views/afficheCandidat.php";
       break;

      //Affiche le formulaire d'édition d'un client
      case "formEditCandidat":
        //On prépare la vue a afficher avec les données dont elle a besoin
        $candidatRepo = new CandidatRepository();
        $candidat = $candidatRepo->getOneById($pdo, $_GET['id']);

        $statutRepo = new StatutRepository();
        $listStatut = $statutRepo->getStatut($pdo);

        $siteRepo = new SiteRepository();
        $listSite = $siteRepo->getSite($pdo);

        $formationRepo = new FormationRepository();
        $listFormation = $formationRepo->getFormation($pdo);

        $civiliteRepo = new CiviliteRepository();
        $listCivilite = $civiliteRepo->getCivilite($pdo);

        $vueAAfficher = "views/formEditCandidat.php";
        break;

      //Met à jour les données d'un client dans la bdd
      case "updateCandidat":
        //Instancier un objet du modèle qui va s'occuper de sauvegarder votre client
        $candidat = new Candidat();
        $candidat->setIne($_POST["ine"]);
        $candidat->setCivilite($_POST["civilite"]);
        $candidat->setNom($_POST["nom"]);
        $candidat->setPrenom($_POST["prenom"]);
        $candidat->setNomJf($_POST["nomJf"]);
        $candidat->setDateNaiss($_POST["dateNaiss"]);
        $candidat->setLieuNaiss($_POST["lieuNaiss"]);
        $candidat->setDptNaiss($_POST["dptNaiss"]);
        $candidat->setNationalite($_POST["nationalite"]);
        $candidat->setAdresse($_POST["adresse"]);
        $candidat->setDomicile($_POST["domicile"]);
        $candidat->setCp($_POST["cp"]);
        $candidat->setVille($_POST["ville"]);
        $candidat->setPays($_POST["pays"]);
        $candidat->setTel1($_POST["tel1"]);
        $candidat->setTel2($_POST["tel2"]);
        $candidat->setEmail($_POST["email"]);
        $candidat->setNiveau($_POST["niveau"]);
        $candidat->setDiplome($_POST["diplome"]);
        $candidat->setEtablissement($_POST["etablissement"]);
        $candidat->setAnnee($_POST["annee"]);
        $candidat->setStatut($_POST["id_statut"]);
        $candidat->setSite($_POST["id_site"]);
        $candidat->setSouhait1($_POST["id_souhait"]);
        $candidat->setSouhait2($_POST["id_souhait_1"]);
        $candidat->setSouhait3($_POST["id_souhait_2"]);
        $candidat->setObservation($_POST["observation"]);

        //On sauveagrde et on prépare la vue à afficher ensuite
        $message = $candidat->save($pdo);
        $vueAAfficher = "views/formEditCandidat.php";
        break;

      //Supprime un client dans la bdd
      case "deleteCandidat":
        //Instancier l'objet modèle client à partir duquel on va supprimer son enregistrement dans la bdd

        $candidat = new Candidat();
        $candidat->setId($_GET['id']);

        $message = $candidat->delete($pdo);
        $candidatRepo = new CandidatRepository();
        $listCandidat = $candidatRepo->getAll($pdo);
        $vueAAfficher = "views/listCandidat.php";
        break;

      default:
  		  if(empty($_SESSION['login'])) {
  			  $vueAAfficher = "views/login.php";
        } else {
  			  //On prépare la vue a afficher avec les données dont elle a besoin
  			  $adminRepo = new AdminRepository();
  			  //$listeCandidat = $adminRepo->getAll($pdo);

          $siteRepo = new SiteRepository();
          $listSite = $siteRepo->getSite($pdo);

          $statutRepo = new StatutRepository();
          $listStatut = $statutRepo->getStatut($pdo);

          $formationRepo = new FormationRepository();
          $listFormation = $formationRepo->getFormation($pdo);

          $civiliteRepo = new CiviliteRepository();
          $listCivilite = $civiliteRepo->getCivilite($pdo);

  			  $vueAAfficher = "formulaire.php";
  			  break;
  		  }

  }

  include_once('layouts/layout.php');
