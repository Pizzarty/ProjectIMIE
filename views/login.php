<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Inscription</title>
	<link rel="stylesheet" type="text/css" href="web/css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>


<body class="body">

	<div class="container">
		<div class="row decal">
			<div class="col-lg-3 col-xs-0"></div>
			<div class="col-lg-2 col-xs-12">
				<a href="http://imie-ecole-informatique.fr/" title="Retourner vers le site"><img src="./web/img/imie.png" id="imgacceuil"></a>
			</div>
			<div class="col-lg-4 col-xs-12">
				<div class="butn but1">S'inscrire / Postuler</div>
				<div class="butn but2">Se connecter</div>
				<form action="./index.php" method="POST">
					<input type="text" placeholder="Nom d'utilisateur" name="login" class="champlog">
					<input type="password" placeholder="Mot de passe" name="pwd" class="champlog">
					<input type="submit" value="Connexion"/>
					<label><?php if(isset($message)) echo $message ?></label>
					<input type="hidden" name="action" value="verifLogin"/>
				</form>

				<div class="col-lg-3"></div>

			</div>
		</div>
	</div>
	<script src="web/js/javascript.js"></script>
</body>



</html>
