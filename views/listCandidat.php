<table>
  <thead>
    <th>Id</th>
    <th>Civilité</th>
    <th>Nom</th>
    <th>Nom jeune fille</th>
    <th>Prénom</th>
    <th>Code Postal</th>
  </thead>
  <tbody>
    <?php
    foreach ($listCandidat as $candidat) {
      echo '<tr>';
      echo '<td>' . $candidat->getId() . '</td>';
      echo '<td>' . $candidat->getCivilite() . '</td>';
      echo '<td>' . $candidat->getNom() . '</td>';
      echo '<td>' . $candidat->getPrenom() . '</td>';
      echo '<td>' . $candidat->getCp() . '</td>';
      echo '<td><a href="./index.php?action=formEditCandidat&id=' . $candidat->getId() . '"">Editer</a></td>';
      echo '<td><a href="./index.php?action=deleteCandidat&id=' . $candidat->getId() . '">Supprimer</a></td>';
      echo '</tr>';
    }
    ?>
  </tbody>
</table>
<!-- Afficher ici le message d'erreur ou de confirmation lors d'une suppression -->
<label><?php if(isset($message)) echo $message ?></label>
