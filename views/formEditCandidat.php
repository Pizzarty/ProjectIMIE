<form action="./index.php" method="POST">

    <label>Civilité</label>
    <select name="civilite">
      <?php foreach ($listCivilite as $civilite){ ?>
      <option value="<?php echo $civilite->getId()?>" <?php echo ($civilite->getCivilite() == $candidat->getCivilite() ?  'selected' : '' )?>>
        <?php
          echo $civilite->getCivilite();
        ?>
      </option>
      <?php } ?>
    </select><br>

    <label>Nom</label>
    <input type="text" name="nom" value="<?php echo $candidat->getNom() ?>"/>
    <br>

    <label>Prénom</label>
    <input type="text" name="prenom" value="<?php echo $candidat->getPrenom() ?>"/>
    <br>

    <label>Date de Naissance</label>
    <input type="text" name="dateNaiss" value="<?php echo $candidat->getDateNaiss() ?>"/>
    <br>

    <label>Code Postal</label>
    <input type="text" name="cp" value="<?php echo $candidat->getCp() ?>"/>
    <br>

    <label>Ville</label>
    <input type="text" name="ville" value="<?php echo $candidat->getVille() ?>"/>
    <br>

    <label>Tel 1</label>
    <input type="text" name="tel1" value="<?php echo $candidat->getTel1() ?>"/>
    <br>

    <label>Email</label>
    <input type="text" name="email" value="<?php echo $candidat->getEmail() ?>"/>
    <br>

    <label>Diplome</label>
    <input type="text" name="diplome" value="<?php echo $candidat->getDiplome() ?>"/>
    <br>

    <label>Etablissement</label>
    <input type="text" name="etablissement" value="<?php echo $candidat->getEtablissement() ?>"/>
    <br>

		<label>Statut</label>
    <select name="statut">
      <?php foreach ($listStatut as $statut){ ?>
      <option value="<?php echo $statut->getId()?>" <?php echo ($statut->getLibelle() == $candidat->getStatut() ?  'selected' : '' )?>>
        <?php
          echo $statut->getLibelle();
        ?>
      </option>
      <?php } ?>
    </select><br>

    <label>Site</label>
    <select name="site">
      <?php foreach ($listSite as $site){ ?>
      <option value="<?php echo $site->getId()?>" <?php echo ($site->getVille() == $candidat->getSite() ?  'selected' : '' )?>>
        <?php
          echo $site->getVille();
        ?>
      </option>
      <?php } ?>
    </select><br>

    <label>Souhait1</label>
    <select name="id_souhait">
      <?php foreach ($listFormation as $souhait){ ?>
      <option value="<?php echo $souhait->getId()?>" <?php echo ($souhait->getLibelle() == $candidat->getSouhait() ?  'selected' : '' )?>>
        <?php
          echo $souhait->getLibelle();
        ?>
      </option>
      <?php } ?>
    </select><br>

    <label>Souhait2</label>
    <select name="id_souhait_1">
      <?php foreach ($listFormation as $souhait){ ?>
      <option value="<?php echo $souhait->getId()?>" <?php echo ($souhait->getLibelle() == $candidat->getSouhait1() ?  'selected' : '' )?>>
        <?php
          echo $souhait->getLibelle();
        ?>
      </option>
      <?php } ?>
    </select><br>

    <label>Souhait3</label>
    <select name="id_souhait_3">
      <?php foreach ($listFormation as $souhait){ ?>
      <option value="<?php echo $souhait->getId()?>" <?php echo ($souhait->getLibelle() == $candidat->getSouhait2() ?  'selected' : '' )?>>
        <?php
          echo $souhait->getLibelle();
        ?>
      </option>
      <?php } ?>
    </select><br>

    <input type="submit" value="Modifier"/>
    <br>

    <label><?php if(isset($message)) echo $message ?></label>
    <input type="hidden" name="id" value="<?php echo $candidat->getId() ?>"/>
    <input type="hidden" name="action" value="updateCandidat"/>
    <?php echo '<td><a href="./index.php">Retour</a></td>'; ?>
</form>
