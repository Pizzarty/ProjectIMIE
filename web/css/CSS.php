    <meta charset="utf8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IMIE</title>
    <!-- Bootstrap -->

    <link href="./vendor/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./vendor/css/font-awesome.min.css">
    <link rel="stylesheet" href="./vendor/css/animate.css">
    <link href="./vendor/css/prettyPhoto.css" rel="stylesheet">
    <link href="./vendor/css/style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="./web/js/javascript.js"></script>
